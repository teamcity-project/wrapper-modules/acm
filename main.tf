terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.20"
  }
}

provider "aws" {
  region = var.region
}

locals {
  # Use existing (via data source) or create new zone (will fail validation, if zone is not reachable)
  use_existing_route53_zone = var.use_existing_route53_zone
  route53_zone_domain_name  = var.route53_zone_domain_name
  certificate_domain_name   = var.certificate_domain_name

  # Removing trailing dot from domain - just to be sure :)
  route53_zone_domain_name_cleaned = trimsuffix(local.route53_zone_domain_name, ".")

  create_cerificate_condition = (local.route53_zone_domain_name_cleaned != "" || var.route53_zone_id != "") && (local.certificate_domain_name != "")

  computed_route53_zone_id = local.create_cerificate_condition ? coalescelist(data.aws_route53_zone.this.*.zone_id, aws_route53_zone.this.*.zone_id)[0] : ""
}

data "aws_route53_zone" "this" {
  count = local.create_cerificate_condition && local.use_existing_route53_zone ? 1 : 0

  name         = local.route53_zone_domain_name_cleaned
  private_zone = false
}

resource "aws_route53_zone" "this" {
  count = local.create_cerificate_condition && (! local.use_existing_route53_zone) ? 1 : 0

  name = local.route53_zone_domain_name_cleaned
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 2.0"

  create_certificate = local.create_cerificate_condition

  domain_name = local.certificate_domain_name
  zone_id     = var.route53_zone_id != "" ? var.route53_zone_id : local.computed_route53_zone_id

  subject_alternative_names          = var.subject_alternative_names
  wait_for_validation                = var.wait_for_validation
  validate_certificate               = var.validate_certificate
  validation_allow_overwrite_records = var.validation_allow_overwrite_records
  validation_method                  = var.validation_method

  tags = var.tags
}
