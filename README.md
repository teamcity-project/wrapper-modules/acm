# AWS Certificate Manager wrapper module for Teamcity project

This module is wrapper for [terraform-acm](https://github.com/terraform-aws-modules/terraform-aws-acm) module.

The module:
  - creates ACM certificate for specified domain and SAN names
  - validates the certificate via email or DNS (the validation completed automatically if Route53 controls the domain zone)

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.20 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| certificate\_domain\_name | Main DNS domain name used for the issued cerificate | `string` | `""` | no |
| environment | Environment name | `string` | `""` | no |
| region | AWS region | `string` | n/a | yes |
| route53\_zone\_domain\_name | The name of Route53 hosted zone to contain record for DNS validation | `string` | `""` | no |
| route53\_zone\_id | The ID of the hosted zone to contain record for DNS validation | `string` | `""` | no |
| subject\_alternative\_names | A list of domains that should be SANs in the issued certificate | `list(string)` | `[]` | no |
| tags | A mapping of tags to assign to the resource | `map(string)` | `{}` | no |
| use\_existing\_route53\_zone | Use existing Route53 zone or create new one | `bool` | `true` | no |
| validate\_certificate | Whether to validate certificate by creating Route53 record | `bool` | `true` | no |
| validation\_allow\_overwrite\_records | Whether to allow overwrite of Route53 records | `bool` | `true` | no |
| validation\_method | Which method to use for validation. DNS or EMAIL are valid | `string` | `"DNS"` | no |
| wait\_for\_validation | Whether to wait for the validation to complete | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| acm\_certificate\_arn | The ARN of the certificate |
| acm\_certificate\_domain\_validation\_options | A list of attributes to feed into other resources to complete certificate validation. Can have more than one element, e.g. if SANs are defined. Only set if DNS-validation was used. |
| acm\_certificate\_validation\_emails | A list of addresses that received a validation E-Mail. Only set if EMAIL-validation was used. |
| distinct\_domain\_names | List of distinct domains names used for the validation. |
| validation\_domains | List of distinct domain validation options. This is useful if subject alternative names contain wildcards. |
| validation\_route53\_record\_fqdns | List of FQDNs built using the zone domain and name. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
