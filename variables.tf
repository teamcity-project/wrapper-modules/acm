variable "region" {
  description = "AWS region"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "route53_zone_id" {
  description = "The ID of the hosted zone to contain record for DNS validation"
  type        = string
  default     = ""
}

variable "route53_zone_domain_name" {
  description = "The name of Route53 hosted zone to contain record for DNS validation"
  type        = string
  default     = ""
}

variable "certificate_domain_name" {
  description = "Main DNS domain name used for the issued cerificate"
  type        = string
  default     = ""
}

variable "subject_alternative_names" {
  description = "A list of domains that should be SANs in the issued certificate"
  type        = list(string)
  default     = []
}

variable "wait_for_validation" {
  description = "Whether to wait for the validation to complete"
  type        = bool
  default     = true
}

variable "validate_certificate" {
  description = "Whether to validate certificate by creating Route53 record"
  type        = bool
  default     = true
}

variable "validation_allow_overwrite_records" {
  description = "Whether to allow overwrite of Route53 records"
  type        = bool
  default     = true
}

variable "validation_method" {
  description = "Which method to use for validation. DNS or EMAIL are valid"
  type        = string
  default     = "DNS"
}

variable "use_existing_route53_zone" {
  description = "Use existing Route53 zone or create new one"
  type        = bool
  default     = true
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}
